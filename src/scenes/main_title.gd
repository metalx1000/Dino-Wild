extends Node2D

var a = false

func _ready():
	yield(get_tree().create_timer(.2),"timeout")
	a = true

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") && a:
		$AnimationPlayer.play("fadeout")
		
func next():
	get_tree().change_scene("res://scenes/levels/test_level.tscn")
