extends KinematicBody2D

var active = false
var dir = -1
var speed = 1000
var gravity = 3000

var velocity = Vector2.ZERO

var cat_sprites = [
	"res://objects/cats/black_001.png",
	"res://objects/cats/blue_001.png",
	"res://objects/cats/brown_001.png",
	"res://objects/cats/creme_001.png",
	"res://objects/cats/dark_001.png",
	"res://objects/cats/grey_001.png",
	"res://objects/cats/grey_tabby_001.png",
	"res://objects/cats/orange_tabby_001.png",
	"res://objects/cats/pink_001.png",
	"res://objects/cats/Seal_Point_001.png"
]

func _ready():
	randomize()
	$Sprite.texture = load(cat_sprites[randi() % cat_sprites.size()])
	speed = rand_range(1000,4000)
	var t = rand_range(1,4)
	yield(get_tree().create_timer(t),"timeout")
	wake()

func _process(delta):
	if active:
		$AnimationPlayer.play("walk")
		#velocity.x = 0
		velocity.x = dir * speed * delta
		
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

func turn():
	dir *= -1
	randomize()
	var turn_time = rand_range(3,8)
	$turn_timer.wait_time = turn_time
	
	if dir < 0:
		$Sprite.flip_h = false
	else:
		$Sprite.flip_h = true

func wake():
	$AnimationPlayer.play("wake")

func activate():
	active = true
	$turn_timer.start()
