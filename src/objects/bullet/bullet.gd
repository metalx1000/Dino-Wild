extends Area2D

var speed = 250
var direction = 1
var parent 

func _ready():
	if direction == -1:
		$Sprite.flip_h = true
	pass # Replace with function body.

func _process(delta):
	position.x += speed * direction * delta
	
func _on_bullet_body_entered(body):
	if body == parent:
		return
		
	if body.has_method("take_damage"):
		print(owner)
		body.take_damage
		
	queue_free()
